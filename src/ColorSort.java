public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Color[] balls) {
      
	   Color[] model = {Color.red, Color.green, Color.blue};
	   i: for (int i = 0, mod = 0; i < balls.length && mod < model.length; i++) {
		   if (balls[i] == model[mod])
			  continue;
		   for (int j = i + 1; j < balls.length; j++) {
			   if (balls[j] == model[mod]) {
				   Color t = balls[i];
				   balls[i] = balls[j];
				   balls[j] = t;
				   continue i;
			   }
		   }
		   
		   mod++;
		   i--;
	   }
   }
}